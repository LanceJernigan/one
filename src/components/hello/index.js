import React from 'react';

import style from './style.css';

const Hello = () => (
  <div
    className={style.wrapper}
  >
    <h1>👌</h1>
    <p>Gotcha</p>
  </div>
);

export default Hello;